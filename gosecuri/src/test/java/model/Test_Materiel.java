package model;

import static org.junit.Assert.*;
import org.junit.Test;

public class Test_Materiel {
	
	@Test
	public void test_materiel_creation_notnull() {
		// teste juste le constructeur vide
		Materiel materiel = new Materiel();
		junit.framework.Assert.assertNotNull(materiel);
	}
	
	@Test
	public void test_materiel_ajout_materiel_existant() {
		// teste le constructeur, ajoute qq items, teste si ca somme bien les items
		Materiel materiel = new Materiel();
		materiel.addXitems("bandeau", (long) 5);
		materiel.addXitems("bandeau", (long) 5);
		org.junit.Assert.assertEquals((long) materiel.getTableitem().get("bandeau"), (long) 10);
	}
	
	@Test
	public void test_materiel_taille_stock_total() {
		// test le constructeur, teste le nombre total d'items 
		Materiel materiel = new Materiel();
		materiel.addXitems("bandeau", (long) 5);
		materiel.addXitems("lampe", (long) 5);
		assertEquals(materiel.getNumberItems(), (long) 2);
	}

}
