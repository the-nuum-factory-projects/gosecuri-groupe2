package model;

import static org.junit.Assert.*;
import org.junit.Test;

import java.util.Date;


public class Test_Image {
	
	@Test
	public void test_image_creation_notnull() {
		// test de creation simple avec constructeur vide.
		Image image = new Image(1, new Date(), "foobar");
		assertNotNull(image);
	}

}
