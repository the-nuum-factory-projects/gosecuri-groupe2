package model;

import static org.junit.Assert.*;
import org.junit.Test;

public class Test_Item {
	
	// default public constructor (obligatoire sinon erreur quand le test est cr)
	public Test_Item() {
		super();
	}
	
	@Test
	public void test_item_creation_notnull() {
		// teste juste le constructeur vide
		Item item = new Item();
		junit.framework.Assert.assertNotNull(item);
	}
	
	@Test
	public void test_item_creation_count() {
		// teste le constructeur complet et le getter
		Item item = new Item("bandeau", (long) 10);
		Long result = item.getCount();
		assertEquals(result.intValue(), 10); // expres faux!
	}
	
	@Test
	public void test_item_creation_name() {
		// test le constructeur vide et le setter de nom 
		Item item = new Item();
		item.setName("bandeau");
		String result = item.getName();
		assertEquals(result, "bandeau");
	}

}
