package model;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class Test_User {
		
	private static User JeanRemi;
	private static User BigBoss;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("init");
		JeanRemi = new User();
		BigBoss = new User(007, "Bond", "James", "agent secret", "jamesB", "goldeneye", new Materiel(), "---imagebase64---", true);
	}

	@Test
	public void test_user_creation_notnull() {
		// teste juste le constructeur vide
		assertNotNull(JeanRemi);
	}
	
	@Test
	public void test_user_setadmin() {
		// teste le constructeur complet, user admin, on le vire d'admin, on verifie qu'il est plus admin
		BigBoss.setAdmin(false);
		assertFalse(BigBoss.getAdmin());
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		BigBoss = null;
		JeanRemi = null;
	}
	
	
}
