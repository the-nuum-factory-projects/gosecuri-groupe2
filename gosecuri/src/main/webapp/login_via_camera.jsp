<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!doctype html>

<html lang="en">
<head>
<meta charset="UTF-8"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Photomaton</title>
<link rel="stylesheet" type="text/css" href="./style/style.css">
	<script type="text/javascript" src="./lib/jquery-3.4.1.min.js"></script>
	<script type="text/javascript" src="./lib/webcam.js"></script>
	<script type="text/javascript" src="./js/custom.js"></script>
</head>
<body>

<!-- Design interface header -->
<header id="header">
	<div style="text-align: center">
		<a href="accueil"><img src="img/logo propre-01.png" alt="Logo Go Securi"
		height="122px" width="150px"/></a>
	</div>
</header>

<h2>Presentez-vous face � la camera</h2>

<div class="center-div-cam">

<!-- Espace de prise de vue webcam -->

<fieldset>
	<!--Camera avec vue live, prise de vue-->
	<div class="container-cam" id="Cam">
		<b>Vue de la webcam</b>
		<div id="my_camera" class="camera-container"></div>
		<form>
			<input type="button" id="btnSnapit" value="Figer l'image" >
		</form>
	</div>

	<!--Image figée de l'agent, résultat-->
	<div class="container-prev" id="Prev">
		<b>Resultat</b>
		<div id="results">
			<img id="base64image" src="" /></div>
			<c:if test="${    empty sessionScope.agent.login}"><button type="button" id="btnLogin" >Me loguer</button></c:if>
			<c:if test="${not empty sessionScope.agent.login}"><button type="button" id="btnAvatar" >Mettre � jour mon avatar</button></c:if>
			<c:if test="${not empty sessionScope.agent.login}"><button type="button" id="btnFaceDatabase" >Ajouter � ma base de visages</button></c:if>
		</div>

</fieldset>

<br>
<br>
<br>
	<div id="footer" class="footer-accueil">
		Powered by M.A.C.C.E.'s
	</div>	

</body>
</html>