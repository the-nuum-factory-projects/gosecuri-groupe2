$(document).ready(function() {
	//Configure the camera settings
	function ShowCam() {
		Webcam.set({
			width : 320,
			height : 240,
			image_format : 'jpeg',
			jpeg_quality : 100
		});
		Webcam.attach('#my_camera');
	}
	
	//ask for camera and set settings
	ShowCam();

	//action when button btnSnapit is clicked
	$('#btnSnapit').click(function() {

		Webcam.snap(function(data_uri) {
			//fill the img tag src with received data
			$('#base64image')[0].src = data_uri;
		});
	});

	//action when button btnLogin is clicked
	$('#btnLogin').click(function() {
		// get the image
		var file = $('#base64image')[0].src;
		var formdata = new FormData();
		formdata.append("base64image", file);
		// add another parameter
		var tmpName = "SomeThingYouWant";
		//send all information to the servlet at imgUpload 
		$.ajax({
			  type: "POST",
			  url: "/gosecuri/login_via_camera",
			  data: {
					"name" : tmpName,
					"img" : file
				},
			  success: uploadcomplete(event),
			})
	});
	//action when button btnAvatar is clicked
	$('#btnAvatar').click(function() {
		// get the image
		var file = $('#base64image')[0].src;
		var formdata = new FormData();
		formdata.append("base64image", file);
		// add another parameter
		var tmpName = "SomeThingYouWant";
		//send all information to the servlet at imgUpload 
		$.ajax({
			  type: "POST",
			  url: "/gosecuri/update_avatar",
			  data: {
					"name" : tmpName,
					"img" : file
				},
			  success: uploadcomplete(event),
			})
	});
	//action when button btnFaceDatabase is clicked
	$('#btnFaceDatabase').click(function() {
		// get the image
		var file = $('#base64image')[0].src;
		var formdata = new FormData();
		formdata.append("base64image", file);
		// add another parameter
		var tmpName = "SomeThingYouWant";
		//send all information to the servlet at imgUpload 
		$.ajax({
			  type: "POST",
			  url: "/gosecuri/add_face_to_database",
			  data: {
					"name" : tmpName,
					"img" : file
				},
			  success: uploadcomplete(event),
			})
	});

	function uploadcomplete(event) {
		// say when image is saved
		alert(" Image saved !!");
	}
	
	

});
