<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!doctype html>
<html lang="fr">
	<head>
		<meta charset="UTF-8"/>
		<link rel="stylesheet"href="style/style.css"/>
		<title>Go Securi Authentification</title>
	</head>
	<body>

		<div id="global">
		</div>
		
		<!--  statut de la session  -->
		<div id="connexion" class="top_nav">
		<c:choose>
			<c:when test="${empty sessionScope.agent.login}">Non connect�</c:when>
			<c:otherwise>
				Compte: ${sessionScope.agent.login}
					<!-- Avatar centr� -->
					<div style="text-align: right">
						<img src="img/avatar.jpg" alt="pas d'avatar" height="100px" width="100px"/>
					</div>				
				</c:otherwise>
		</c:choose>
		</div>
		
	<header id="header">
		<div style="text-align: center">
			<a href="accueil"><img src="img/logo propre-01.png" alt="Logo Go Securi"
			height="122px" width="150px"/></a>
		</div>
	</header>

