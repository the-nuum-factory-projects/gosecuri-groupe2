<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!doctype html>
<html lang="fr">
<head>
<meta charset="UTF-8"/>
<link rel="stylesheet" href="style/style.css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<title>Restitution de mat�riel</title>
</head>
<body>

<jsp:include page="header.jsp"/>
	
<br>
<br>

<h2>Veuillez restituer votre mat�riel selon la quantit� emprunt�e</h2>
<br>

<div class="center-div">

<fieldset> <!-- Balise pour definir un cadre autour des elements -->
<div id="restitution" class="inventaire">

	<!--Espace pour les boutons et cadre d'enregistrement d'un nouveau membre-->
	<div class="login_container">
	<form method="POST" action="restitution">
        <br>
        <table class="table .table .w-auto">
	        <c:forEach items="${CurrentAgentInventory}" var="item">
	        	<tr>
	        		<td class="col-md-4"></td>
	        		<td class="col-md-2">${item.key}</td>
	        		<c:choose>
        				<c:when test="${item.value > 0}">
	        				<td class="col-md-2"><input type="number" min="0" max="${item.value}" value="${item.value}" name="${item.key}"/></td>
	        			</c:when>
	        			<c:otherwise>
	        				<td class="col-md-2"><em style="color:grey">**Pas emprunt�**</em></td>
	        			</c:otherwise>
	        		</c:choose>
	        		<td class="col-md-4"></td>
	        	</tr>
	        </c:forEach>
        </table>
        </fieldset>
        
		<input type="submit" class="submitBtn" value="Valider" onclick="confirmation()">
	</form>
	</div>
	
</div>
	

	
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>	
<script>
	(function($) {
	$.fn.spinner = function() {
	this.each(function() {
	var el = $(this);
	
	// add elements
	el.wrap('<span class="spinner"></span>');     
	el.before('<span class="sub">-</span>');
	el.after('<span class="add">+</span>');
	
	// substract
	el.parent().on('click', '.sub', function () {
	if (el.val() > parseInt(el.attr('min')))
	el.val( function(i, oldval) { return --oldval; });
	});
	
	// increment
	el.parent().on('click', '.add', function () {
	if (el.val() < parseInt(el.attr('max')))
	el.val( function(i, oldval) { return ++oldval; });
	});
	    });
	};
	})(jQuery);
	
	$('input[type=number]').spinner();
</script>

<script>

function confirmation() {
	  confirm("�tes-vous s�r de votre choix ?");
	}

</script>

</div>
<br>

	<div id="footer" class="footer-accueil">
		Powered by M.A.C.C.E.'s
	</div>	

</body>
</html>	