<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!doctype html>
<html lang="fr">
<head>
<meta charset="UTF-8"/>
<link rel="stylesheet" href="style/style.css" />
<title>Ajout d'un agent</title>
</head>
<body>

<jsp:include page="header.jsp"/>
	
<br>

	<!--Classe de style pour centrer les �l�ments dans une "div"-->
<div class="center-div-new">

<h2>Inscription d'un nouvel agent</h2>
<br>

<fieldset>

	<!--Espace pour les boutons et cadre d'enregistrement d'un nouveau membre-->
	<div class="login_container">
	<form method="POST" action="ajout-agent">
		<table>
			<tr>
				<td>Nom</td>
				<td><input type="text" name="nom" value=""></td>
				<td>*</td>
			</tr>
			<tr>
				<td>Prenom</td>
				<td><input type="text" name="prenom" value=""></td>
				<td>*</td>
			</tr>
			<tr>
				<td>Poste</td>
				<td><input type="text" name="poste" value=""></td>
				<td>*</td>
            </tr>
            <tr>
				<td>Login</td>
				<td><input type="text" name="login" value=""></td> <!--  enlev� tous les pr�remplis ${agent.login} car ca provoque une erreur remplac� par "" -->
				<td>*</td>
			</tr>
			<tr>
				<td>Mot de passe</td>
				<td><input id="pass1" type="password" name="password" value="" /></td>
				<td>*</td>
			</tr>
			<tr>
				<td>Administrateur?</td>
				<td><input id="isAdmin" type="checkbox" name="isAdmin" /></td>
				<td>*</td>
			</tr>

			<tr>
				<td></td>
				<td><input type="submit" class="submitBtn" value="Valider">
				<!--  <input type="reset" value="Annuler" />  --></td>
				<td></td>
			</tr>	
        </table>
        
		<p class="erreur">
		${erreur}		
		</p> 
	</form>
	</div>
	
</fieldset>
	
</div>	

<a class="primaryBtn login" href="https://firebase.google.com/">Consultation des log</a>
<a class="primaryBtn login" href="list-agents">Gestion des utilisateurs</a>

	<div id="footer" class="footer-accueil">
		Powered by M.A.C.C.E.'s
	</div>	

</body>
</html>