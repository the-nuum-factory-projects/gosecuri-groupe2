<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>	

<!doctype html>
<html lang="fr">
<head>
<meta charset="UTF-8"/>
<link rel="stylesheet" href="style/style.css" />
<title>Liste des agents</title>
</head>
<body>
<jsp:include page="header.jsp"/>
<br>

	<!--Classe de style pour centrer les elements dans une "div"-->
<div class="center-div-new">

<h2>Liste des agents</h2>
<br>

<div class="list_container">
 	<table class="table .table .w-auto">
 	<tr><th>Nom de l'agent</th></tr>
 		<c:forEach items="${listUser}" var="user">
 			<tr><td> ${user.nomAgent}</td></tr>	
 		</c:forEach>
 	</table>

</div>
</div>


<div id="footer" class="footer-accueil">
		Powered by M.A.C.C.E.'s
	</div>	
</body>
</html>