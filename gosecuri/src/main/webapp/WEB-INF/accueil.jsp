<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet"href="style/style.css"/>
<title>Authentification</title>
</head>
<body>
		<!--  statut de la session  -->
		<div id="connexion" class="top_nav">
		<c:choose>
			<c:when test="${empty sessionScope.agent.login}">Non connect�</c:when>
			<c:otherwise>
				Compte: ${sessionScope.agent.login}
					<!-- Avatar centr� -->
					<div class="top_nav" style="text-align: right">
						<img src="img/avatar.jpg" alt="pas d'avatar" height="100px" width="100px"/>
					</div>				
				</c:otherwise>
		</c:choose>
		</div>
		
	<div id="main">
<br>
<br>
	<!-- Logo centr� -->
	<div style="text-align: center">
	<img src="img/logo propre-01.png" alt="Logo Go Securi"
	height="244px" width="300px"/>
	
<br>
<br>
	<!-- Boutons de login -->
			<div id="loginBar">
				<div class="Login">
				<a class="primaryBtn login" href="login_via_camera">Utiliser la Webcam</a>
				<br>
				<a class="primaryBtn login" href="authloginmdp">S'identifier avec un login / mot de passe</a>
				<br>
				<br>
				<c:if test="${sessionScope.isAdmin}">
					<a class="primaryBtn login" href="ajout-agent">Espace administrateur</a>
				</c:if>
				<br>
				<br>
					<!--  statut de la session et grisage du bouton onglet inventaire -->
					<c:choose>
						<c:when test="${empty sessionScope.agent.login}"></c:when>
						<c:otherwise>
							<a class="primaryBtn login" href="inventory">Emprunter du mat�riel</a>
							<a class="primaryBtn login" href="restitution">Restituer du mat�riel</a>
							<br>
							<br>
							<a class="primaryBtn login" href="disconnect">Deconnexion</a>
						</c:otherwise>
					</c:choose>

				</div>
			</div>
	
	</div>
<br>
<br>
<br>
<br>
<br>
</div>
	<div id="footer" class="footer-accueil">
		Powered by M.A.C.C.E.'s
	</div>	
	


</body>
</html>

