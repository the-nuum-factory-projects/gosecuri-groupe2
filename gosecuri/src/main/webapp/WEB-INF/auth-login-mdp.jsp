<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!doctype html>
<html lang="fr">
<head>
<meta charset="UTF-8" />
<link rel="stylesheet" href="style/style.css" />
	<script type="text/javascript" src="./js/popup-connexion.js"></script>
<title>Authentification classique</title>
</head>
<body>
<jsp:include page="header.jsp"/>
	
<br>

<h2>Veuillez vous identifier</h2>

<br>

	<!--Classe de style pour centrer les elements dans une "div"-->
<div class="center-div-log">

	<c:if test="${not empty message_non}">
		<div id="popup_bloc">
			<span>${message_non}</span>
		</div>
	</c:if>

<fieldset>

	<!--Espace pour les boutons et cadre de texte de login-->
	<div class="login_container">
		<form method="POST" action="authloginmdp">
			<table>
				<tr>
					<td>Login</td>
					<td><input type="text" name="user_login" value="votre login"></td>
				</tr>
				<tr>
					<td>Mot de passe</td>
					<td><input type="password" name="user_password" value="******"></td>
				</tr>
				<tr>
					<td></td>
					<td><input id="submitBtn" type="submit" class="submitBtn" onclick="openPopup()" value="Envoyer"></td>
				</tr>
			</table>
		</form>
	</div>
	
</fieldset>
<br>
	
</div>
	
	<div id="footer" class="footer-accueil">
		Powered by M.A.C.C.E.'s
	</div>	
	
	
	
</body>
</html>