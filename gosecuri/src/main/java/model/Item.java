package model;

// objet Item (ex: des gants, une veste, un pistolet, un casque, une brassi�re, etc
// c'est un bean qui est utile pour �changer des infos entre l'inventaire et les objets s�lectionn�s par l'utilisateur.


public class Item {
	private String name;
	private Long count;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getCount() {
		return count;
	}
	public void setCount(Long count) {
		this.count = count;
	}
	 
	public Item(String name, Long count) {
		super();
		this.name = name;
		this.count = count;
	}
	
	public Item() {
		super();
	}
	 
}
