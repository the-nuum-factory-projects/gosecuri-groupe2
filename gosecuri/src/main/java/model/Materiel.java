package model;

import java.util.HashMap; 
// normalement un objet mat�riel correspond � l'inventaire total du magasin
public class Materiel {
	private HashMap<String, Long> tableitem = new HashMap<String, Long>() ;
	// par exemple: <paires de gants: 25, menottes: 6>, etc le integer est le stock du magasin
	public Materiel(HashMap<String, Long> tableitem) {
		super();
		this.tableitem = tableitem; 
	}
	
	public Materiel() {
		super();
	}
	
	// attention elle remplace le compte existant par le nouveau compte, ou ajoute un objet qui n'existait pas.
	public void addOneItem(String item_id, Long current_stock) {
		this.tableitem.put(item_id, current_stock);
	}
	
	// celle ci ajouter X au stock existant de l'item
	public void addXitems(String item_id, Long stock_to_add) {
		Long current_stock = this.tableitem.getOrDefault(item_id, new Double(0).longValue());
		this.tableitem.put(item_id, stock_to_add + current_stock);
	}
	
	public int getNumberItems() {
		return(this.tableitem.size());
	}
	
	public HashMap<String, Long> getTableitem() {
		return this.tableitem;
	}
	
	public void setTableitem(HashMap<String, Long> tableitem) {
		this.tableitem = tableitem;
	}
	
	
}
