package model;

import java.util.Date;

// la classe image n'est pas pour le moment utilis�e...
// initialemnt dans le modele de don�es on avait mis le match user-image uniquemnet dans les objets images.
public class Image {
	int id;
	Date date; 
	String url; 
	String base64;
	String id_agent; 
	
	// construire avec un string base64
	public Image(int id, Date date, String base64) {
		super();
		this.id = id;
		this.date = date;
		this.base64 = base64;	
		// option: creer un string base64
	}
	
	public String getIdagent() {
		return this.id_agent;
	}
	public void setIdagent(String id_agent) {
		this.id_agent = id_agent;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public Date getDate() {
		return date;
	}

	public void setDate( Date date) {
		this.date = date;
	}
	
	public String getUrl() {
		return url;
	}

	public void setUrl( String url) {
		this.url = url;
	}
	
	public String getBase64() {
		return this.base64;
	}
	public void setBase64( String base64) {
		this.base64 = base64;
	}
	
}
