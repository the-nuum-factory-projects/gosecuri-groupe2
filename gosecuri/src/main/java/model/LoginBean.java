package model;

// C'est un "bean" qui contient login/mot de passe et qui est cr�e � chaque fois qu'une personne 
// essaye de se connecter � la page d'authentification login/mdp

public class LoginBean {
	private String login;
	private String pass;
	private boolean admin;
	
	public String getLogin() {
		return login;
	}
	public boolean isAdmin() {
		return admin;
	}
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
}

