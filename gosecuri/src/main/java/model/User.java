package model;

import java.util.HashMap;

// la classe user est tr�s utilisee car elle stocke l'utilisateur en cours en train d'interagir dans l'application
// utilis� pour cr�er des nouveaux utilisateurs par l'admin ou pour en authentifier.
public class User {
	private int idAgent;
	private String nomAgent;
	private String prenomAgent;
	private String poste;
	private String login;
	private String motdepasse;
	private Materiel items;
	private String image;
	private boolean isAdmin;
	
	
	
	// ce constructeur est utilis� par firebase pour recuperer un utilisateur de la base cloud
	public User() {
		super();
	}

	// ce constructeur est surtout utilise pour cr�er de nouveaux agents. (admin)
	public User(int idAgent, String nomAgent, String prenomAgent, String poste, String login, String motdepasse,
			Materiel items, String image, boolean isAdmin) {
		super();
		this.idAgent = idAgent;
		this.nomAgent = nomAgent;
		this.prenomAgent = prenomAgent;
		this.poste = poste;
		this.login = login;
		this.motdepasse = motdepasse;
		this.items = items;
		this.image = image;
		this.isAdmin = isAdmin;
	}

	public String getImage() {
		return image;
	}
	
	public void setImage(String image) {
		this.image = image;
	}

	public boolean getAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public int getIdAgent() {
		return idAgent;
	}

	public void setIdAgent(int idAgent) {
		this.idAgent = idAgent;
	}

	public String getNomAgent() {
		return nomAgent;
	}

	public void setNomAgent(String nomAgent) {
		this.nomAgent = nomAgent;
	}

	public String getPrenomAgent() {
		return prenomAgent;
	}

	public void setPrenomAgent(String prenomAgent) {
		this.prenomAgent = prenomAgent;
	}

	public String getPoste() {
		return poste;
	}

	public void setPoste(String poste) {
		this.poste = poste;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getMotdepasse() {
		return motdepasse;
	}

	public void setMotdepasse(String motdepasse) {
		this.motdepasse = motdepasse;
	}

	public Materiel getItems() {
		return this.items;
	}

	public void setItems(Materiel items) {
		this.items = items;
	}

	@Override
	public String toString() {
		return "User [idAgent=" + idAgent + ", nomAgent=" + nomAgent + ", prenomAgent=" + prenomAgent + ", poste="
				+ poste + ", login=" + login + ", motdepasse=" + motdepasse + ", items=" + items + "]";
	}

}
