package tools;

public class UserDataChecker {
	
	public static boolean isPwdCompliant(String pwd) {
		
		if(pwd.length()<5) {
			return false;
		}else if(pwd.isEmpty()) {
			return false;
		}
		
		return true;
	}
}
