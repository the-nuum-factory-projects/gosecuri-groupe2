package com.gosecuri.firebase.controller;
import org.apache.commons.codec.digest.DigestUtils;

public class PasswordEncryptionController {
	public String Encrypt(String PasswordInClear) {
		String result = (String) DigestUtils.sha256Hex(PasswordInClear);
		return result;
	}
}
