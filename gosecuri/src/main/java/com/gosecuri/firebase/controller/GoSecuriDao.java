/**
 * 
 */
package com.gosecuri.firebase.controller;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;

import model.Item;
import model.Materiel;
import model.User;


public class GoSecuriDao {

	private Firestore db;
	// la GosecuriDAO est une classe que l'on instancie pour cr�er la connection avec la firebase.
	// on passe le path recuperer pour le servletcontext.
	public GoSecuriDao(String path) {

		FileInputStream serviceAccount;
		try {
			serviceAccount = new FileInputStream(path + "gosecuri-groupe2-firebase-adminsdk-xh93h-45e6008268.json");

			FirebaseOptions options = new FirebaseOptions.Builder()
					.setCredentials(GoogleCredentials.fromStream(serviceAccount))
					.setDatabaseUrl("https://gosecuri-groupe2.firebaseio.com").build();
			// Les lignes suivantes emp�chent de cr�er plusieurs connections � la fois (code SO) pour acc�l�rer ou emp�cher une erreur
			// https://stackoverflow.com/a/47387163/3871924
			FirebaseApp firebaseApp = null;
			List<FirebaseApp> firebaseApps = FirebaseApp.getApps();
			if (firebaseApps != null && !firebaseApps.isEmpty()) {
				for (FirebaseApp app : firebaseApps) {
					if (app.getName().equals(FirebaseApp.DEFAULT_APP_NAME))
						firebaseApp = app;
				}
			} else
				firebaseApp = FirebaseApp.initializeApp(options);
			this.db = FirestoreClient.getFirestore();
			System.out.println("Connect� � Firebase !");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void addUser(User a) {
		// m�thode pour ajouter document utilisateur dans la firebase. la classe user contient les m�mes champs que dans le document firebase
		// cr�er un "futur" ( = doc en attente) pour ensuite �crire dedans.
		ApiFuture<com.google.cloud.firestore.WriteResult> future = db.collection("New User").document(a.getLogin())
				.set(a);
		try {

			// Appel synchronis� et affichage dans la console du message de confirmation qui va en meme temps cl�turer le future.
			System.out.println("Mise-�-jour de " + a.getLogin() + " effectu�e le " + future.get().getUpdateTime());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}

	public void deleteUser(User a) {
		// m�thode pour supprimer document utilisateur dans la firebase. la classe user contient les m�mes champs que dans le document firebase
		// cr�er un "futur" ( = doc en attente) pour ensuite �crire dedans.
		ApiFuture<com.google.cloud.firestore.WriteResult> future = db.collection("New User").document(a.getLogin()).delete();
		try {

			// Appel synchronis� et affichage dans la console du message de confirmation qui va en meme temps cl�turer le future.
			System.out.println("Suppression de " + a.getLogin() + " effectu�e le " + future.get().getUpdateTime());

		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}

	public void updateAvatar(User a, String image) {
		// m�thode pour ajouter une image dans la base de donn�es des visages d'un utilisateur
		// utilis� par la servlet add_face_to_db
		ApiFuture<com.google.cloud.firestore.WriteResult> future = db.collection("New User").document(a.getLogin())
				.update("image", image);
		try {
			System.out.println("Mise-�-jour de la photo de  " + a.getLogin() + "; effectu�e le " + future.get().getUpdateTime());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}

	public void addFaceToDatabase(User a, String image) {
		// m�thode pour ajouter une image de visage de l'utilisateur aux images de training sous la forme texte base64 avec la webcam.
		// il faut mettre le document sous forme d'une map (liste) sinon l'attribut n'a pas de nom !
		HashMap<String, String> imageObject = new HashMap<String, String>() {{
			put("base64", image);// l'image base64
			put("id", a.getLogin()); // le login de l'user
		}};
		ApiFuture<com.google.cloud.firestore.WriteResult> future = db.collection("images").document().set(imageObject);
		try {
			System.out.println("Ajout d'une nouvelle photo dans Firebase de " + a.getLogin() + "; effectu� le " + future.get().getUpdateTime());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}

	public void deleteFaceToDatabase(User a, String image) {
		// m�thode pour supprimer une image de visage de l'utilisateur aux images de training sous la forme texte base64 avec la webcam.
		// il faut mettre le document sous forme d'une map (liste) sinon l'attribut n'a pas de nom !

		//code � modifier et rajouter
		//Firebase pas bdd relationnelle. Du coup il faut trouver une m�thode pour requ�ter et ainsi obtenir toutes les photos d'un agent.
		/*HashMap<String, String> imageObject = new HashMap<String, String>() {{

		}};
		ApiFuture<com.google.cloud.firestore.WriteResult> future = db.collection("images").document().delete(imageObject);
		try {
			System.out.println("Suppression des photos dans Firebase de " + a.getLogin() + ";  effectu�e le " + future.get().getUpdateTime());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}*/
	}

	public HashMap<String, Integer> SaveAllFacesFromDatabase(String path_to_write) throws IOException {
		// m�thode qui va �crire dans le file system chaque image de la base Image sous forme d'un .jpeg
		// l'algo de reconnaissance OBLIGE � ce que les images d'entrainement aient un nom avec un label num�rique "1-..." "2-..." devant.
		// Il faut donc boucler sur chaque image pour faire une liste (map) avec le nom + label.
		try {
			ApiFuture<QuerySnapshot> future = db.collection("images").get();
			List<QueryDocumentSnapshot> images = future.get().getDocuments();

			// Initialiser les variables de la boucle images
			String filename;
			Integer current_label_counter = 0;
			Integer image_name_counter = 100;
			HashMap<String, Integer> UserLabelDictionnary = new HashMap<String, Integer>(); // annuaire des utilisateurs d�j� connus.

			// Boucle sur chaque image de la collection:
			for (QueryDocumentSnapshot image : images) {

				// V�rifier si l'user a d�j� un label num�rique attribu�
				if (UserLabelDictionnary.containsKey(image.get("id"))) {

					// si on a d�j� une image de cet utilisateur on conserve son label pour la prochaine fois
					//m�me label utilis� pour toutes les images d'un utilisateur x
					current_label_counter+=0; // inchang�
					filename = UserLabelDictionnary.get(image.get("id")).toString() + "-" + image_name_counter;
				} else {
					// si on voit cet user pour la premiere fois
					current_label_counter+=1; // on incr�mente et on ajoute � l'annuaire
					UserLabelDictionnary.put((String) image.get("id"), current_label_counter);
					filename = current_label_counter.toString() + "-" + image_name_counter;
				}

				// On enregistre l'image
				(new ImagesController()).Base64ImageToLocalImgFolder((String) image.get("base64"), path_to_write, filename);

				// Un num�ro d'increment pour jamais avoir 2 images de m�me nom...
				image_name_counter += 1;
			}
			return UserLabelDictionnary;
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return null;
	}

	// M�thode pour �craser le stock perso d'un agent avec le mat�riel en argument
	//A chaque emprunt, �a se rajoute � son inventaire
	public void updatePersonnalInventory(User a, Materiel monMateriel) {
		ApiFuture<com.google.cloud.firestore.WriteResult> future = db.collection("New User").document(a.getLogin())
				.update("items", monMateriel.getTableitem());
		try {
			System.out.println("Mise-�-jour de l'historique d'emprunt de " + a.getLogin() + "; effectu�e le " + future.get().getUpdateTime());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}

	// M�thode pour ajouter un log � la collection de logs
	public void logEvent(String myevent) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // attention pas de slashs
		Date date = new Date();

		// il faut mettre le document sous forme d'une map (liste) sinon l'attribut n'a pas de nom!
		HashMap<String, String> logObject = new HashMap<String, String>() {{
			put("log", myevent);
		}};
		String log_name = dateFormat.format(date);
		ApiFuture<com.google.cloud.firestore.WriteResult> future = db.collection("logs").document(log_name)
				.set(logObject);
		try {
			System.out.println("Ajout d'un log pour [" + myevent + "], effectu� le " + future.get().getUpdateTime());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}

	public void takeItem(User a, String nomItem, Long howmany) {
		// m�thode qui joue le role d' INCREMENT ou DECREMENT du nombre d'item d'une cat�gorie dans l'inventaire.
		// en fait ca s'appelle "takeItem", mais en mettant un nombre n�gatif on fait une RESTITUTION
		Long currentValue = this.getStockforItem(nomItem); // utiliser une m�thode DAO pour r�cuperer le stock actuel
		ApiFuture<com.google.cloud.firestore.WriteResult> future = db.collection("Inventory").document(nomItem)
				.update("count", currentValue - howmany); // ici on retire X au stock SI il y a du stock. On ne teste pas la nullit� du stock.
		try {
			System.out.println( "Mise-�-jour de l'inventaire " + howmany + " item(s) " + nomItem + " effectu�e le " + future.get().getUpdateTime());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}

	public User getUser(String loginAgent) {
		// Renvoie un objet de classe User stock� dans la firebase� partir du login (normalement unique) de cet utilsateur.
		try {
			DocumentReference docRef = db.collection("New User").document(loginAgent);
			ApiFuture<DocumentSnapshot> future = docRef.get();
			DocumentSnapshot document;
			document = future.get();
			if (document.exists()) {
				// corriger le format de donn�es d'items pour matcher l'user class
				/*for (Map.Entry<String, Integer> entry : mymap.entrySet()) {
				    System.out.println(entry.getKey() + "/" + entry.getValue());
				}*/

				// correction du hashmap pour le mettre dans l'user  
				User userToReturn = document.toObject(User.class);
				HashMap<String, Long> mymap = (HashMap) document.get("items");
				Materiel tempitems = new Materiel((HashMap) document.get("items"));
				userToReturn.setItems(tempitems);
				return userToReturn;
			} else {
				System.out.println("Utilisateur introuvable !");
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Long getStockforItem(String nomItem) {
		// m�thode qui va recuperer le stock d'un type d'item dans l'inventaire, en vue de le mettre ajour dans une autre m�thode.
		try {
			DocumentReference docRef = db.collection("Inventory").document(nomItem);
			ApiFuture<DocumentSnapshot> future = docRef.get();
			DocumentSnapshot document;
			document = future.get();
			if (document.exists()) {
				System.out.println(document.getData());
				Item a = document.toObject(Item.class);
				return (Long) a.getCount();
			} else {
				System.out.println("Mat�riel introuvable !");
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Materiel getStockAllItems() {
		// methode qui recupere l'ensemble de l'inventaire d'un coup et renvoir un objet de type materiel (avec le hashmap item/count dedans)
		try {
			ApiFuture<QuerySnapshot> future = db.collection("Inventory").get();
			List<QueryDocumentSnapshot> documents = future.get().getDocuments();
			Materiel TemporaryInventory = new Materiel();
			for (QueryDocumentSnapshot document : documents) {
				TemporaryInventory.addOneItem(document.getId(), document.getLong("count").longValue());
			}
			return(TemporaryInventory);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return null;		
	}

	public ArrayList<User> getAllUsers() {
		try {
		// methode qui recupere l'ensemble des utilisateurs d'un coup 
			ApiFuture<QuerySnapshot> future = db.collection("New User").get();
			List<QueryDocumentSnapshot> documents = future.get().getDocuments();
			ArrayList<User> userList = new ArrayList<User>();
			for (QueryDocumentSnapshot document : documents) {
				userList.add(document.toObject(User.class));
			}
			return(userList);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return null;		
	}

}
