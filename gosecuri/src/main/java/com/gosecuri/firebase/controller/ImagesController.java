package com.gosecuri.firebase.controller;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;

public class ImagesController {
	
	public void AuthLocalSave(String path, String myimage64) {
		// on stocke l'image qu'on veut authentifier dans le dossier /img (il faut remonter d'un cran dans le dossier)
		
	}
	
	public void TrainLocalSave(String path, String myimage64) {
		// on stocke l'image qu'on veut authentifier dans le dossier /img (il faut remonter d'un cran dans le dossier)
		
	}
	
	// convertir image de base64 (sortie webcam) en byte[] (lisible par le syst�me)
	public byte[] base64ImageToBytes(String encodedImg) {
		String base64_data = encodedImg.split(",")[1];
		byte[] decodedImg = Base64.getDecoder()
                .decode(base64_data .getBytes(StandardCharsets.UTF_8));
		return decodedImg;
	}
	
	// convertir image base64 en fichier image dans le syst�me (JPG)
	public void Base64ImageToLocalImgFolder(String base64Image, String path, String filenameSansExtension) throws IOException {
		Path destinationFile = Paths.get(path, filenameSansExtension + ".jpg");
		Files.write(destinationFile, base64ImageToBytes(base64Image));
	}
	
	public void Base64ImageToTextFileDump(String base64Image, String path, String filenameSansExtension) throws IOException {
		Path destinationFile = Paths.get(path, filenameSansExtension + ".jpg");
		FileWriter writer = new FileWriter("d:/temp/" + filenameSansExtension + ".txt");
        writer.write(base64Image);
        writer.close();
	}
	
	// Not in use anymore
	public void saveBase64Img(String encodedImg, String path, String TextOrImage, String filenameSansExtension) {
		//System.out.println(encodedImg);	
		String base64_data=encodedImg.split(",")[1];
		byte[] decodedImg = Base64.getDecoder()
                .decode(base64_data .getBytes(StandardCharsets.UTF_8));
		Path destinationFile = Paths.get(path, filenameSansExtension + ".jpg");
		try {
			if (TextOrImage.equals("Text")) {
				FileWriter writer = new FileWriter("d:/temp/" + filenameSansExtension + ".txt");
                writer.write(encodedImg);
                writer.close();
			} else {
				Files.write(destinationFile, decodedImg);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
}



