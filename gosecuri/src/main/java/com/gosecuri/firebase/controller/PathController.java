package com.gosecuri.firebase.controller;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

public class PathController {
	public String getRelativePath(HttpServletRequest myrequest) { // Dependency injection
		ServletContext context = myrequest.getServletContext();
		String relative_path = context.getRealPath("/WEB-INF/");
		return relative_path;
	}
}
