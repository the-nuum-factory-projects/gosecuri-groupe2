package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;

import com.gosecuri.firebase.controller.GoSecuriDao;
import com.gosecuri.firebase.controller.PathController;

import model.LoginBean;
import model.User;

/**
 * Servlet implementation class servlet_add_face_to_database
 */
@WebServlet(name="/servlet_add_face_to_database", urlPatterns = {"/add_face_to_database", "/gosecuri/add_face_to_database"})
public class servlet_add_face_to_database extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public servlet_add_face_to_database() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// l'objet session permet d'indiquer si l'utilisateur est connect� ou non
		HttpSession session = request.getSession();

		// sauver en base Firebase si la personne est bien connect�e (mais normalemnet c'est le cas puisque le bouton pour arriver ici n'existe que si connect�!)
		if (session.getAttribute("agent") != null) {

			// cr�er la connection firebase
			String relative_path = (new PathController()).getRelativePath(request);
			GoSecuriDao addFaceDao = new GoSecuriDao(relative_path);

			// Ajoute au document firebase de l'utilisateur son image en format texte.
			addFaceDao.addFaceToDatabase((User) session.getAttribute("agent"), request.getParameter("img")); 

			// upload a log event
			addFaceDao.logEvent("FACE ADDED TO COLLECTION for agent: " + ((LoginBean) session.getAttribute("agent")).getLogin());
		} else {
			JOptionPane.showMessageDialog(null, "Face Image non mis � jour: veuillez vous connecter d'abord", "Erreur d'identifiants", JOptionPane.WARNING_MESSAGE);
		}
	}

}
