package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;

import com.gosecuri.firebase.controller.GoSecuriDao;
import com.gosecuri.firebase.controller.PathController;

import model.LoginBean;
import model.User;

/**
 * Servlet implementation class servlet_update_avatar
 */
@WebServlet(name="/servlet_update_avatar", urlPatterns = {"/update_avatar", "/gosecuri/update_avatar"})
public class servlet_update_avatar extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public servlet_update_avatar() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// L'objet session permet d'indiquer si l'utilisateur est connect� ou non
		HttpSession session = request.getSession();
		
		// Sauver en base Firebase si la personne est bien connect�e (mais normalement c'est le cas puisque le bouton pour arriver ici n'existe que si connect�!)
		if (session.getAttribute("agent") != null) {
			
			// Cr�er la connection firebase
			String relative_path = (new PathController()).getRelativePath(request);
			GoSecuriDao updateAvatarDao = new GoSecuriDao(relative_path);
			
			// Ajout au document firebase de l'utilisateur, son image en format texte.
			updateAvatarDao.updateAvatar((User) session.getAttribute("agent"), request.getParameter("img")); 
			
			// upload a log event
			updateAvatarDao.logEvent("AVATAR IMAGE UPDATED for agent: " + ((LoginBean) session.getAttribute("agent")).getLogin());
		} else {
			JOptionPane.showMessageDialog(null, "Avatar non mis � jour: veuillez vous connecter d'abord", "Erreur d'identifiants",
					JOptionPane.WARNING_MESSAGE);
		}
	}

}
