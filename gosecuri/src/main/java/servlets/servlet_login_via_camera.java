package servlets;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;

import com.gosecuri.firebase.controller.GoSecuriDao;
import com.gosecuri.firebase.controller.ImagesController;
import com.gosecuri.firebase.controller.PathController;
import com.gosecuri.firebase.controller.faceRecoController;

import model.User;

/**
 * Il s'agit de la servlet pour la page de la webcam.
 */
@WebServlet(name="/servlet_login_via_camera", urlPatterns = {"/login_via_camera", "/gosecuri/login_via_camera"})
public class servlet_login_via_camera extends HttpServlet {
	private static final long serialVersionUID = 1L;

       
    public servlet_login_via_camera() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/login_via_camera.jsp").forward(request, response); // ici on n'est pas dans webinf (?)
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// l'objet session permet d'indiquer si l'utilisateur est connect� ou non
		HttpSession session = request.getSession();
		boolean authenticate = false;
		
		// Cr�ation de la connection firebase
		String relative_path = (new PathController()).getRelativePath(request);
		System.out.println(relative_path);
		GoSecuriDao loginCameraDao = new GoSecuriDao(relative_path);
		
		// Algorithme reconnaissance:
		String path_to_save_images = relative_path + File.separator + ".." + File.separator + "img" + File.separator + "training_dir";
		HashMap<String, Integer> UserLabelDictionnary = loginCameraDao.SaveAllFacesFromDatabase(path_to_save_images);
			
		// On fabrique le chemin o� l'on stocke l'image de la camera
		String new_relative_path = (new PathController()).getRelativePath(request) + File.separator + ".." + File.separator + "img";
		
		// On stocke l'image de la cam�ra sous le nom "image_to_predict"
		(new ImagesController()).Base64ImageToLocalImgFolder(request.getParameter("img"), new_relative_path, "image_to_predict");
		
		// On appelle l'algorithme de prediction
		HashMap<String, Double> prediction = (new faceRecoController()).Implement(request, "image_to_predict.jpg", new_relative_path);
			
		// Lors de la pr�diction, l'IA nous donne un num�ro de correspondance ( =ID). Le dictionnaire indique l'ID et son nom.
		//Pour retrouver le nom de l'agent on fait une recherche par ID et non par nom 
		//= on doit 'inverser" le dictionnaire ( = liste des utilisateurs) pour pouvoir le chercher.
		HashMap<Integer, String> LabelUserDictionnary = new HashMap<Integer, String>();
		for(Map.Entry<String, Integer> entry : UserLabelDictionnary.entrySet()){
			LabelUserDictionnary.put(entry.getValue(), entry.getKey());
		}
		
		// V�rification dans la console
		System.out.println("labeluser");
		LabelUserDictionnary.entrySet().forEach(entry->{
		    System.out.println(entry.getKey() + " " + entry.getValue());  
		 });
		
		// Condition sur la confiance de prediction (c'est des pourcentages ou quasi):
		if (prediction.get("confidence") < 50) {
			JOptionPane.showMessageDialog(null, "Le visage n'est pas suffisament bien reconnu. R�essayer.", "Erreur algorithme",
					JOptionPane.WARNING_MESSAGE);
		} else { // c ok!
			authenticate = true; 
			
			// On va chercher l'utilisateur dont le login est donn� par le match du label dans le nouveau dictionnaire.
			String authenticated_login = LabelUserDictionnary.get(prediction.get("label").intValue());
			System.out.println(authenticated_login);
			User RequestedUser = loginCameraDao.getUser(authenticated_login);
			
			// upload a log event
			loginCameraDao.logEvent("LOGIN SUCCESS at login: " + authenticated_login);
			
			// On attribue une session admin � l'agent ( dans le cas o� il est admin)
			session.setAttribute("agent", RequestedUser);
			session.setAttribute("isAdmin", (boolean) RequestedUser.getAdmin());
			
			// On stocke alors son avatar dans le dossier img (il faut remonter d'un cran dans le dossier)
			String image = RequestedUser.getImage();
			
			//String new_relative_path = relative_path + File.separator + ".." + File.separator + "img";
			//System.out.println(new_relative_path);
			
			// Si l'image de l'user n'existe pas, on supprime celle qui �tait d�j� �ventuellemnet pr�sente.
			if (image.isEmpty()) {
				File file = new File(new_relative_path + File.separator+ "avatar.jpg"); 
				file.delete();
			} else { // on enregistre sinon l'image dans le dossier WEBINF "Img":
				(new ImagesController()).Base64ImageToLocalImgFolder(image, new_relative_path, "avatar");
			}
			
			//authenticatePopup(authenticate, newlogin.getLogin()); // un pop-up (en arri�re plan) et on renvoie � l'accueil
			
			this.getServletContext().getRequestDispatcher("/WEB-INF/accueil.jsp").forward(request, response);

		}
		
		// Pas encore fait:
		// supprimer les photos t�l�charg�es, de tous les utilisateurs.
	}


	
}
