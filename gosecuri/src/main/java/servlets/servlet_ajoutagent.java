package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.gosecuri.firebase.controller.GoSecuriDao;
import com.gosecuri.firebase.controller.PathController;

//import model.Item;
import model.User;
import tools.UserDataChecker;


/**
 * Servlet implementation class ajout-agent. C'est la page pour que les admins ajoutent des nouvelles recrues!
 */
@WebServlet(name="/servlet_ajoutagent", urlPatterns = {"/ajout-agent", "/gosecuri/ajout-agent"})
public class servlet_ajoutagent extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor. 
	 */
	public servlet_ajoutagent() {

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// tester si l'utilisateur est admin sinon renvoyer � la page d'accueil direct.
		HttpSession session = request.getSession();
		if (session.getAttribute("isAdmin") == null || (boolean) session.getAttribute("isAdmin") == false) {
			this.getServletContext().getRequestDispatcher("/WEB-INF/accueil.jsp").forward(request, response);
		}

		// Envoyer la page
		this.getServletContext().getRequestDispatcher("/WEB-INF/ajout-agent.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//L'objet session permet d'indiquer si l'utilisateur est connect� ou non
		HttpSession session = request.getSession();

		// Un message d'erreur appara�t sur la page si des champs sont manquants. 
		String erreur = "";
		String nom = request.getParameter("nom");
		if(nom.isEmpty()) {
			erreur += "Vous devez renseigner un nom<br>";
		}
		String prenom = request.getParameter("prenom");
		if(prenom.isEmpty()) {
			erreur += "Vous devez renseigner un prenom<br>";
		}
		String poste = request.getParameter("poste");
		if(poste.isEmpty()) {
			erreur += "Vous devez renseigner un poste<br>";
		}
		String login = request.getParameter("login");
		if(login.isEmpty()) {
			erreur += "Vous devez renseigner un nom d'utilisateur<br>";
		}
		boolean isAdmin = request.getParameter("isAdmin") != null;

		// Chiffrage (cryptage) du mot de passe d�s qu'on le recoit pour ne jamais le stocker en clair.
		String pwd_notChecked=request.getParameter("password");
		String pwd_Checked;
		User agent=null;
		if(UserDataChecker.isPwdCompliant(pwd_notChecked)) {
			pwd_Checked=pwd_notChecked;
			String password = org.apache.commons.codec.digest.DigestUtils.sha256Hex(pwd_Checked);
			
			// Instanciation d'un nouvel agent.
			agent = new User(124, nom, prenom, poste, login, password, null, "", isAdmin);
		}else {
			erreur +="le paswword ne respecte pas les contraintes<br>";
		}
		
		
//		if(password.isEmpty()) {
//			erreur += "Vous devez renseigner un mot de passe<br>";
//		}

		

		//Commande pour enregistrer un agent dans firebase et renvoyer � l'accueil
		if(erreur.isEmpty()){

			// session.setAttribute("agent", agent);
			String relative_path = (new PathController()).getRelativePath(request);
			GoSecuriDao newagent = new GoSecuriDao(relative_path);
			newagent.addUser(agent);

			// upload a log event
			newagent.logEvent("AGENT ADDED by admin: " + ((User) session.getAttribute("agent")).getLogin() + " for agent: " + agent.getLogin());

			this.getServletContext().getRequestDispatcher("/WEB-INF/accueil.jsp").forward(request, response);
		}

		else {
			request.setAttribute("erreur", erreur);
			request.setAttribute("agent", agent);
			this.getServletContext().getRequestDispatcher("/WEB-INF/ajout-agent.jsp").forward(request, response);
		}
	}
}