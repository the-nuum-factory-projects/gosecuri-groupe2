package servlets;

import java.io.File;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.gosecuri.firebase.controller.PathController;

import model.User;
/**
 * Servlet implementation class servlet_deconnexion
 */
@WebServlet(name="/servlet_disconnect", urlPatterns = {"/disconnect", "/gosecuri/disconnect"})
public class servlet_disconnect extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public servlet_disconnect() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		// Supprimer l'avatar de l'utilisateur stock� localement,
		String image = ((User) session.getAttribute("agent")).getImage();
		String relative_path = (new PathController()).getRelativePath(request);
		String new_relative_path = relative_path + File.separator + ".." + File.separator + "img";

		// si l'image de l'user existe, on la supprime
		if (!image.isEmpty()) {
			File file = new File(new_relative_path + File.separator+ "avatar.jpg"); 
			file.delete();
		}
		// et enfin, suppression des connexions.
		session.setAttribute("isAdmin", false);
		session.setAttribute("agent", null);
		this.getServletContext().getRequestDispatcher("/WEB-INF/accueil.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
