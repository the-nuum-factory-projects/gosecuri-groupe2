package servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.gosecuri.firebase.controller.GoSecuriDao;
import com.gosecuri.firebase.controller.PathController;

import model.Materiel;
import model.User;

/**
 * Servlet implementation class servlet_list_agents
 */

@WebServlet(name="/servlet_list_agents", urlPatterns = {"/list-agents", "/gosecuri/list-agents"})
public class servlet_list_agents extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public servlet_list_agents() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// tester si l'utilisateur est admin sinon renvoyer � la page d'accueil direct.
		HttpSession session = request.getSession();
		if (session.getAttribute("isAdmin") == null || (boolean) session.getAttribute("isAdmin") == false) {
			this.getServletContext().getRequestDispatcher("/WEB-INF/accueil.jsp").forward(request, response);
		}

		// Envoyer � la page HTML la liste d'utilisateurs 	
		GoSecuriDao fetchUsers = new GoSecuriDao(request.getServletContext().getRealPath("/WEB-INF/"));
		ArrayList<User> CurrentUsers = fetchUsers.getAllUsers();
		request.setAttribute("listUser",CurrentUsers);

		// Affiche la page mise-�-jour
		this.getServletContext().getRequestDispatcher("/WEB-INF/list-agents.jsp").forward(request, response);
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// l'objet session permet d'indiquer si l'utilisateur est connect� ou non
		HttpSession session = request.getSession();

		// mise-�-jour de l'inventaire
		User currentUser = (User) session.getAttribute("admin");
		if (currentUser != null) {

			// Cr�er ou recr�er la connection firebase
			String relative_path = (new PathController()).getRelativePath(request);
			GoSecuriDao listAgentsDao = new GoSecuriDao(relative_path);

			//Relier � la JSP
			ArrayList<User> CurrentUsers = new ArrayList<User>();   
			request.setAttribute("list", CurrentUsers);
			request.getRequestDispatcher("list-agents.jsp").forward(request,response);
		}

	}
}
