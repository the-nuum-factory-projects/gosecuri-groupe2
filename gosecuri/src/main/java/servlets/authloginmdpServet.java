package servlets;

import java.io.File;
import java.io.IOException;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.gosecuri.firebase.controller.GoSecuriDao;
import com.gosecuri.firebase.controller.ImagesController;
import com.gosecuri.firebase.controller.PathController;

import model.LoginBean;
import model.User;

/**
 * Servlet implementation class authloginmdpServet
 * IL s'agit de la servlet pour la page d'authentification login/mdp des utilisateurs. 
 */
@WebServlet(name = "/authloginmdpServet", urlPatterns = { "/authloginmdp", "/gosecuri/authloginmdp" })
public class authloginmdpServet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public authloginmdpServet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/WEB-INF/auth-login-mdp.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// l'objet session permet de passer l'utilisateur connect� ou non.
		HttpSession session = request.getSession();
		// on prepare � mettre les 2 entr�es utilisateur (login et mdp) dans un bean.
		LoginBean newlogin = new LoginBean();
		newlogin.setLogin((String) request.getParameter("user_login"));
		newlogin.setPass((String) org.apache.commons.codec.digest.DigestUtils.sha256Hex(request.getParameter("user_password")));

		// cr�er la connection firebase
		String relative_path = (new PathController()).getRelativePath(request);
		GoSecuriDao checkAgent = new GoSecuriDao(relative_path);

		// on tente de r�cup�rer l'utilisateur connect�, �a renvoie null s'il n'existe pas.
		User RequestedUser = checkAgent.getUser(newlogin.getLogin());

		// boolean pour savoir si un utilisateur est logu� ou pas
		boolean authenticate = false;		
		if (RequestedUser == null) {
			authenticate = false;
			session.setAttribute("agent", null);
			authenticatePopup(authenticate, newlogin.getLogin());

			// upload a log event
			checkAgent.logEvent("LOGIN FAILED at login: " + newlogin.getLogin());
			
			String msg_non = "Identifiants incorrects, veuillez recommencer";
			request.setAttribute("message_non", "Identifiants incorrects,\n veuillez recommencer");

			this.getServletContext().getRequestDispatcher("/WEB-INF/auth-login-mdp.jsp").forward(request, response);
			
		} else if (newlogin.getPass().equals(RequestedUser.getMotdepasse())) {

			// ici on a bien la concordance entre le mot de passe du bean et  celui de firebase, c'est OK! 
			authenticate = true; 

			// upload a log event  
			checkAgent.logEvent("LOGIN SUCCESS at login: " + newlogin.getLogin());

			// on cr�er une session pour l'agent, et une sp�cifique pour l'admin
			session.setAttribute("agent", RequestedUser);
			session.setAttribute("isAdmin", (boolean) RequestedUser.getAdmin());

			// on stocke alors son avatar (sa photo) dans le dossier "img" (il faut remonter d'un cran dans le dossier)
			String image = RequestedUser.getImage();
			String new_relative_path = relative_path + File.separator + ".." + File.separator + "img";

			// si l'image de l'user n'existe pas, on supprime celle qui etait d�j� �ventuellement pr�sente.
			if (image.isEmpty()) {
				File file = new File(new_relative_path + File.separator+ "avatar.jpg"); 
				file.delete();
			} else { // on enregistre sinon l'image dans le dossier WEBINF "Img":
				(new ImagesController()).Base64ImageToLocalImgFolder(image, new_relative_path, "avatar");
			}
			//authenticatePopup(authenticate, newlogin.getLogin()); // un pop-up (en arreire plan) et on renvoie a l'accueil 

			authenticatePopup(authenticate, newlogin.getLogin()); // un pop-up (en arri�re plan) et on renvoie a l'accueil
			String msg_oui = "Authentification r�ussie";
			request.setAttribute("message_oui", msg_oui);

			this.getServletContext().getRequestDispatcher("/WEB-INF/accueil.jsp").forward(request, response);
		} else {
			authenticate = false;
			//authenticatePopup(authenticate, newlogin.getLogin()); // pop up et on reste sur la meme page: 

			// upload a log event
			checkAgent.logEvent("LOGIN FAILED UNKNOWN ERROR at login: " + newlogin.getLogin());
			
			String msg_non = "Identifiants incorrects, veuillez recommencer";
			request.setAttribute("message_non", msg_non);
			System.out.println(request.getAttribute("message_non"));
			this.getServletContext().getRequestDispatcher("/WEB-INF/auth-login-mdp.jsp").forward(request, response);
		}

	}
	
		
	public void authenticatePopup(boolean authenticated, String username) {
		if (authenticated == true) {
			System.out.println("Authentification r�ussie");
		}else {
			System.out.println("Identifiants incorrects");
		} 	
	}

	
}
