package servlets;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;

import com.gosecuri.firebase.controller.GoSecuriDao;
import com.gosecuri.firebase.controller.PathController;

import model.Materiel;
import model.User;

/**
 * Servlet implementation class servlet_inventory pour que les agents reservent des objets
 */
@WebServlet(name="/inventory", urlPatterns = {"/inventory", "/gosecuri/inventory"})
public class servlet_inventory extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public servlet_inventory() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// Tester si l'utilisateur est connect� sinon renvoyer � la page d'accueil direct.
		HttpSession session = request.getSession();
		if (session.getAttribute("agent") == null) {
			this.getServletContext().getRequestDispatcher("/WEB-INF/accueil.jsp").forward(request, response);
		}

		// Envoyer � la page HTML la liste du mat�riel disponible.
		GoSecuriDao fetchInventoryStock = new GoSecuriDao(request.getServletContext().getRealPath("/WEB-INF/"));
		Materiel CurrentInventory = fetchInventoryStock.getStockAllItems();
		request.setAttribute("CurrentInventory", CurrentInventory.getTableitem());

		// Affiche la page mise-�-jour
		this.getServletContext().getRequestDispatcher("/WEB-INF/inventory.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// l'objet session permet d'indiquer si l'utilisateur est connect� ou non
		HttpSession session = request.getSession();

		// mise-�-jour de l'inventaire
		User currentUser = (User) session.getAttribute("agent");
		if (currentUser != null) {

			// Cr�er ou recr�er la connection firebase
			String relative_path = (new PathController()).getRelativePath(request);
			GoSecuriDao updateInventoryDao = new GoSecuriDao(relative_path);

			// Mettre dans un objet "materiel" la liste des objets selectionn�es par l'agent (qui sont n�cessairement en stock >0 car on a d�j� test� dans la jsp)
			Materiel InventoryBeingUpdated = currentUser.getItems();

			// R�cup�rer le r�sultat du formulaire sous la forme d'une MAP (liste) d'un coup
			Map<String, String[]> CheckedItems = request.getParameterMap();

			//CheckedItems.forEach((k,v) -> System.out.println(k + ":" + v[0]));
			// louper sur ce hashmap

			for (Map.Entry<String, String[]> entry : CheckedItems.entrySet()) {
				if (!entry.getValue()[0].equals("0")) { // si stock non demand� on passe au suivant

					// Mise-�- jour an ajoutant au stock existant le nouveau compte demand�:
					InventoryBeingUpdated.addXitems(entry.getKey(), Long.parseLong(entry.getValue()[0]));

					// Retirer dans le stock firebase -X items pour cet item:
					updateInventoryDao.takeItem((User) currentUser, entry.getKey(), Long.parseLong(entry.getValue()[0]));
					updateInventoryDao.logEvent("EMPRUNT DE " + (entry.getValue()[0]).toString() + " ARTICLE(S): " + entry.getKey() + " FAIT par agent: " + currentUser.getLogin());

					// on met un sleep (une pause) car sinon �a semble sauter des �critures de log...
					try
					{
						Thread.sleep(300);
					}
					catch(InterruptedException ex)
					{
						Thread.currentThread().interrupt();
					}
				}
			}

			// Mettre � jour l'�quipement personnel de l'utilisateur dans firebase
			updateInventoryDao.updatePersonnalInventory((User) currentUser, InventoryBeingUpdated);
			System.out.println("stock global et user mis a jour dans firebase");

			// on renvoie a l'accueil 
			this.getServletContext().getRequestDispatcher("/WEB-INF/accueil.jsp").forward(request, response);
		} else {
			JOptionPane.showMessageDialog(null, "Inventaire non mis a jour: veuillez vous connecter d'abord", "Erreur d'identifiants",
					JOptionPane.WARNING_MESSAGE);
		}


	}

}
